from django.shortcuts import render

# Create your views here.
def about(request):
    return render(request, 'about.html')

def question_page(request):
    return render(request, 'question.html')