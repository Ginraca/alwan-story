from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "aboutpage"

urlpatterns = [
    path('', views.about, name = "about"),
    path('question/', views.question_page, name = "question_page")
]
