from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "schedule"

urlpatterns = [
    path('', views.list_schedule, name = "list_schedule"),
    path('create-schedule', views.create_schedule, name = "create"),
    path('delete/<id>', views.delete_schedule, name = 'delete')
]
