from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Schedule(models.Model):
    kategori = [
        ('Academic', 'Academic'),
        ('Organization', 'Organization'),
        ('Personal', 'Personal'),
        ('Other', 'Other')
    ]

    day = models.CharField(max_length=9)
    date = models.DateField(null=True, editable=True)   
    hour = models.TimeField(null=True, editable=True)
    activity_name = models.CharField(max_length=20)
    place = models.CharField(max_length=30)
    category = models.CharField(max_length=10, choices=kategori)
    author = models.ForeignKey(User, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.activity_name