from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Schedule
from . import forms

# Create your views here.
def list_schedule(request):
    schedules = Schedule.objects.all()
    response = {
        'schedules': schedules
    }
    return render(request, 'list_schedule.html', response)

def create_schedule(request):
    form = forms.CreateModelSchedule(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.day = obj.date.strftime("%A")
        obj.save()
        return redirect('schedule:list_schedule')
    response = {
        'form': form
    }
    return render(request, 'create_schedule.html', response)

def delete_schedule(request, id):
    schedule = Schedule.objects.filter(id=id)[0]
    schedule.delete()
    return redirect('schedule:list_schedule')