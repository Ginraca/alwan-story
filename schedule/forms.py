from django import forms
from . import models

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class CreateModelSchedule(forms.ModelForm):
    class Meta:
        model = models.Schedule
        fields = [
            'date',
            'hour',
            'activity_name',
            'place',
            'category'  
        ]
        widgets = {
            'date': DateInput(),
            'hour': TimeInput()
        }